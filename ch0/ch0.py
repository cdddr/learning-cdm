
44+11*4-6/11

7 * 24 * 60

2304811 - (2304811 // 47)*47

0 == ((673 + 909) / 3) % 3

x=-9
y=1/2
2**(y+1/2) if x+10<0 else 2**(y-1/2)

{2,1,3}
{1+2,3,"a"}

len({'a', 'b', 'c', 'a', 'a'})

sum({1,2,3})

sum({1,2,3}, 15)

S={1,2,3}
2 in S
4 in S
4 not in S

{1,2,3} | {2,3,4}
{1,2,3} & {2,3,4}

S={1,2,3}
S.add(4)
S.remove(2)
S
S.update({4,5,6})
S
S.intersection_update({5,6,7,8,9})
S
T=S
T.remove(5)
U=S.copy()
U.add(5)
S

{2*x for x in {1,2,3} }

{x**2 for x in {1,2,3,4,5}}

{2**x for x in {0,1,2,3,4}}

{x*x for x in S | {1, 5, 7}}

{x*x for x in S | {1,5,7} if x > 2}

{x*y for x in {1,2,3} for y in {2,3,4}}

{x*y for x in {3,5,6} for y in {7,8,9}}

{x*y for x in {0,1,3} for y in {2,6,18}}

S = {1,5,6,7,10,15,24,89}
T = {1,2,3,4,5,6,7,8,9,10,12}
{x for x in S if x in T}

len([1, 2, 3, [4, 5, 6, 7], {1,2,3,4}])

L=[20,10,15,75]
sum(L)/len(L)

sum([ [1,2,3], [4,5,6], [7,8,9] ], [])

[[x,y] for x in ['A','B','C'] for y in [1,2,3]]

LofL=[[.25, .75, .1], [-1, 0], [4,4,4,4]]
sum(sum(LofL, []))

[x,y,z] = [1,2,3,4]

mylist = [30,20,10]
mylist[1] = 0
mylist

S = {-4,-2,1,2,5,0}
[(i,j,k) for i in S for j in S for k in S if sum((i,j,k)) == 0]

S = {-4,-2,1,2,5,0}
[(i,j,k) for i in S for j in S for k in S if sum((i,j,k)) == 0 if (i,j,k) != (0,0,0)]

S = {-4,-2,1,2,5,0}
[(i,j,k) for i in S for j in S for k in S if sum((i,j,k)) == 0 if (i,j,k) != (0,0,0)][0]

L=[1,2,3,4,4]
len(L)
len(set(L))

range(10)
list(range(10))

{i for i in range(100) if i % 2 == 1}

L = ['A','B','C','D','E']
list(zip(range(5),L))

[sum((x,y)) for (x,y) in zip([10,25,40],[1,15,20])]

dlist = [{'James':'Sean', 'director':'Terence'},{'James':'Roger','director':'Lewis'},{'James':'Pierce','director':'Roger'}]
k='James'
[d[k] for d in dlist if k in d]

dlist = [{'Bilbo':'Ian','Frodo':'Elijah'},{'Bilbo':'Martin','Thorin':'Richard'}]
k='Bilbo'
[d[k] if k in d else 'NOT PRESENT' for d in dlist]

{ k:k*k for k in range(100) }

D={'red','white','blue'}
{ x:x for x in D}

base=10
digits=set(range(base))
maxdigit=max(digits)
max3digit=sum([(base**i*maxdigit) for i in range(3)]) 
{x:[(x // (base**i)) % base for i in reversed(range(3))] for x in range(1000) if x <= max3digit}

id2salary = {0:1000.0, 3:990, 1:1200.50}
names = ['Larry', 'Curly', '', 'Moe']
{names[id]:id2salary[id] for id in id2salary.keys()}

def twice(z): return 2*z

twice(2)
twice('aabb')
twice([0, 1, 2])
z

def nextInts(L): return [x+1 for x in L]

nextInts([1,5,7])

def cubes(L): return [x**3 for x in L]

cubes([1,2,3])

def dict2list(dct,keylist): return [dct[i] for i in keylist]

dict2list({'a':'A', 'b':'B', 'c':'C'}, ['b','c','a'])

def list2dict(L, keylist): return {k:v for (k,v) in zip(keylist, L)}

list2dict(['A', 'B', 'C'], ['a','b','c'])

def all_3_digit_numbers(base, digits): return {x for x in range(1+sum([(base**i*max(digits)) for i in range(3)]))}

all_3_digit_numbers(2, {0,1})
all_3_digit_numbers(3, {0,1,2})
all_3_digit_numbers(10, {0,1,2,3,4,5,6,7,8,9})

import math
help(math)

from random import randint
def movie_review(name): return name + ': ' +['See it!', 'A gem!', 'Ideological claptrap!', 'Five Stars!'][randint(0,3)]

randint(0,100)
randint(0,1000000)
movie_review('Titanic')
movie_review('Slumdog Millionaire')
movie_review('A Beautiful Mind')

import dictutil
from imp import reload
dictutil.dict2list({'a':'A', 'b':'B', 'c':'C'}, ['a','b','c'])

listrange2dict(['A','B','C'])

for x in [1,2,3]:
        y = x*x
        print(y)

def makeInverseIndex(strlist):
        index = {}
        for (docnum,document) in enumerate(strlist):
                f = open(document)
                for line in f:
                        words = line.split()
                        for word in words:
                                if word in index:
                                        index[word].add(docnum)
                                else:
                                        index[word] = {docnum}
        return index

inverseIndex = makeInverseIndex(['stories_small.txt', 'stories_big.txt'])

def orSearch(inverseIdx, query):
        return set.union(*[inverseIdx[q] for q in query])

def andSearch(inverseIdx, query):
    return set.intersection(*[inverseIdx[q] for q in query])

orSearch(inverseIndex, ['poets','when','administrator','Taylor'])
andSearch(inverseIndex, ['poets','when','administrator','Taylor'])

def increments(L): return [i+1 for i in L]

increments([1,2,3])

def cubes(L): return [i**3 for i in L]

cubes([1,2,3])

def tuple_sum(A,B): return [(a+c,b+d) for ((a,b),(c,d)) in zip(A,B)]

tuple_sum([(1,2),(10,20)],[(3,4),(30,40)])

def inv_dict(d): return {v:k for (k,v) in d.items()}

inv_dict({'thank you':'merci', 'goodbye':'au revoir'})

def row(p, n): return [p + i for i in range(n)]

row(10, 4)
[row(j, 20) for j in range(15)]
[[j + i for i in range(20)] for j in range(15)]
